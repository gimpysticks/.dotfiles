alias sii_decrypt='wine /home/sticks/SII_Decrypt/SII_Decrypt.exe'
#alias venv35='deactivate &>/dev/null;source /home/sticks/venv35/bin/activate'
alias jupyterNotebook='deactivate &>/dev/null;source /home/sticks/jupyterNotebook/bin/activate'
#alias venv36='deactivate &>/dev/null;source /home/sticks/venv36/bin/activate'
alias de='deactivate'
alias gitdotfiles='/usr/bin/git --git-dir=/home/sticks/dotfilesgit --work-tree=/home/sticks'
alias arcopkglist='pacman -Qqe'
export DO_TOKEN="ca964ea2ef19f007eeebe48d8ae92c67e802514c87a30cc574b830349577bcb2"
export powerline_root="/usr/local/lib/python3.8/dist-packages/powerline"
alias repower="powerline-daemon -k; sleep 2; powerline-daemon -q"
alias xfcekeybinds="xfconf-query -c xfce4-keyboard-shortcuts -l -v| cut -d'/'
-f4| awk '{printf '%30s', $2; print "\t" $1}' |sort| uniq"
alias ol='vim /home/$USER/.cache/xfce4/notifyd/log'

export PATH=${PATH}:/home/sticks/bin
export PATH=${PATH}:/home/sticks/.local/bin
export PATH=${PATH}:/home/sticks/SII_Decrypt
export PATH=${PATH}:/home/sticks/thunderbird
export PATH=${PATH}:/home/sticks/appimages
export gmailpass="udcfljpnnxctzimp"

export telegram_bot_name='gimpynotifacation_bot'
export telegram_bot_api_token='894327394:AAHpY2eoebJDhYHWse9-DFvu6xr3fcT2pi8'
export telegram_bot_chatID='gimpynotifBot'
export telegram_userId='614272142'
export telegram_username='@gimpysticks'

alias ki='pkill insync'
alias si='insync start'
alias rsob='/usr/bin/openbox --restart'
alias ud='~/bin/ud.sh'
alias sp='~/bin/sp.sh'
alias lo='~/bin/lo.sh'
alias ubuntu_full_update='sudo apt update && sudo apt full-upgrade -y && \
sudo apt clean && sudo apt autoclean && sudo apt autoremove -y'
alias pf='cd ~/pythonfiles'
alias Pp='cd ~/pythonfiles/Python_Projects'
#shuf -n 1 /home/sticks/Documents/gre | cowsay
#/usr/bin/neofetch
export TERM=xterm-256color
export EDITOR='vim'
export VISUAL='vim'
export CHEATCOLORS=true
#Added to save commands
export PROMPT_COMMAND='history -a'
setxkbmap -option "caps:escape"
#===========================================================================
#Load TMUX on Shell Start
#
#if [[ -z "$TMUX" ]]; then
#    if tmux has-session 2>/dev/null; then
#        exec tmux attach
#    else
#        exec tmux
#    fi
#fi
#=============================================================================
#if [ $TILIX_ID ] || [ $VTE_VERSION ] ; then source /etc/profile.d/vte.sh; fi # Ubuntu Budgie END

#===========================================================================
#  Add vi mode to bash
set -o vi
#===========================================================================

#===========================================================================
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/lib/python3.8/site-packages/powerline/bindings/bash/powerline.sh

#===========================================================================
function cozmosdk
{
  cd /home/$USER/cozmo-python-sdk
  source /home/$USER/venv35/bin/activate
}
export -f cozmosdk

function tvpc
{
    /usr/bin/wakeonlan D4:85:64:B5:7A:CA #kilbaneTV
}

export -f tvpc

function hplaptop
{
    /usr/bin/wakeonlan 00:1d:72:79:41:e6 #HpLaptop
}

export -f hplaptop

function kidspc
{
/usr/bin/wakeonlan f0:4d:a2:29:fa:49 #KidsLivingRoom
}
export -f kidspc

alias ducks='du -cks * |sort -rn |head'

#free
alias free="free -mt"

function sd
{
echo -e $USERPASS|sudo -S poweroff
}

export -f sd

function rb
{
echo -e $USERPASS|sudo -S reboot
}

export -f rb

fe() {
  local files
  IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
  [[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
}

source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash
#==============================================================================
#===================================Books======================================

practicalvim () {
    tmux new-session -n 'PracticalVim' -d
    tmux send -t PracticalVim "evince
    /home/sticks/Documents/docs-pdf/Pragmatic_Bookshelf/practical_vim.pdf"
    tmux a
}


